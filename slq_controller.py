#!/usr/bin/env python3

# Sequential Linear Quadratic Programming (SLQ) Controller
# Based off page 19 of https://arxiv.org/abs/1708.09342

import gym
import numpy as np
import time
import math
from utils import stateToAngle
from Model import Model
from scipy.optimize import minimize


class SLQController():
    def __init__(self, rollout_steps):
        self.rollout_steps = rollout_steps

        # guess starting policy
        self.control_policy = np.random.normal(0.0, 0.3, rollout_steps)

        # initialise model
        self.model = Model()

    def control(self, state):

        # first do rollout
        state_trajectory = [state]
        for i in range(0, self.rollout_steps):
            new_state = self.model.step(state, self.control_policy[i])
            state_trajectory.append(new_state)

        # optimise control via backprop
        for i in range(0, self.rollout_steps-1)[::-1]:
            x = np.array(state_trajectory[i], self.control_policy[i])
            result = minmize(self.model.cost_fn, method="BFGS", jac=self.model.cost_fn_der,
                                options={"gtol" : 1e-6, "disp" : True})
            self.control_policy[i] = result.x

#### Environment setup ####
env = gym.make('Pendulum-v0')
env.reset()

# Give a set starting position
# This is done for reproducability
env = env.unwrapped
env.state = np.array([1.57, 0])
############################

#### Experiment setup ####
# initialise state and control
controls = env.action_space.sample()
cost = 0.

# Start simulation loop
for _ in range(1000):
    env.render()
    state, cost, done, info = env.step(controls)
    state = np.array([stateToAngle(state[0], state[1]), state[2]])
    cost = -cost  # convert to classical cost where we aim to get to 0
    print("Theta: {:.2f}  Cost: {:.2f}  Control: {:.2f}".format(state[0], cost, controls[0]))

    # This is added so that it's easier to monitor things
    # time.sleep(0.1)

# stop environment
env.close()
