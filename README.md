# Pendulum Control

This is a playground for me to play around with different control algorithms using the
OpenAI gym environment and Python

## Setup

I use conda envornments as python is messy to control. To install conda check [this out](https://docs.conda.io/projects/conda/en/latest/user-guide/install/linux.html).

To setup an envrionment one:

```bash
conda create -n pendulum python=3.7.4
conda activate pendulum
conda install numpy scipy matplotlib
pip install gym
```

To use:

```bash
conda activate pendulum
```

## Environment

The simplest possible one for classical control - [Pendulum v0](https://github.com/openai/gym/wiki/Pendulum-v0)

### State space

Type: Box(3)

Num | Observation  | Min | Max  
----|--------------|-----|----
0   | cos(theta)   | -1.0| 1.0
1   | sin(theta)   | -1.0| 1.0
2   | theta dot    | -8.0| 8.0

### Control space

Type: Box(1)

Num | Action  | Min | Max  
----|--------------|-----|----
0   | Joint effort | -2.0| 2.0

### Model

```math
\dot{\theta}_{t+1} = \dot{\theta}_t + (-1.5g \times \sin{(\theta_t + \pi)} + 3u)dt
```

```math
\theta_{t+1} = \theta_t + \dot{\theta_{t+1}}dt
```

u is the control which has range [-2,2]

Constants:

```math
dt = 0.05
```

```math
g = 10 ??
```

### Cost function

```math
\theta^2 + 0.1\dot{\theta}^2 + 0.001 u^2
```

Range: `[0, 16.2736044]`

Goal is to achieve 0 usually

## Algorithms

### random_control.py

Does nothing. Just shows how to use the Gym environment.

### pid.py

Simple PID controller to bring the pendulum to controlled state facing downwards. For that the environment needed to be modified slightly.

### slq_controller.py

Sequential Linear Quadratic Programming (SLQ) Controller.
Based off page 19 of https://arxiv.org/abs/1708.

### mppi_control.py

Model Predictive Path Integral Control as inspired by
the Information Theoretic MPC paper

https://homes.cs.washington.edu/~bboots/files/InformationTheoreticMPC.pdf

