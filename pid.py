#!/usr/bin/env python3


# This is a simple pendulum PID controller which aims to bring the 
# pendulum to a stable state facing downwards in the OpenAI gym
# Based on https://medium.com/@mattia512maldini/pid-control-explained-45b671f10bc7

import gym
import numpy as np
import time
import math


# Converts the sin/cos state representation
# to a theta angle state representation
def stateToAngle(cos_theta, sin_theta):
    theta = np.arctan2(sin_theta, cos_theta)
    return theta


# The PID Controller
# It's initialised by giving it a target angle,
# Kp, Ki and Kd constants for the controller
class PID():
    def __init__(self, target, Kp, Ki, Kd):
        self.target = target
        self.Kp = Kp
        self.Ki = Ki
        self.Kd = Kd
        self.max_control = None
        self.min_control = None
        self.error = 0
        self.error_buffer = [0,0,0,0,0,0,0,0,0,0]

    # This is used to constain the control output
    def setConstraints(self, max, min):
        self.max_control = max
        self.min_control = min

    # Calculates the error in the system and stores in buffer
    def calculateError(self, state):
        self.error = self.target - state

        if len(self.error_buffer) >= 10:
            self.error_buffer.pop(0)

        self.error_buffer.append(self.error)

        return self.error

    # Calculates the control needed to stabalise the system
    def control(self):
        p_part = self.Kp * self.error
        i_part = self.Ki * sum(self.error_buffer)
        d_part = self.Kd * (abs(self.error) - abs(self.error_buffer[-2]))

        control = p_part + i_part + d_part

        print("P={:.2f} I={:.2f} D={:.2f}".format(p_part, i_part, d_part))

        # Constrain the control
        if self.max_control is not None:
            if control > self.max_control:
                control = self.max_control

        if self.min_control is not None:
            if control < self.min_control:
                control = self.min_control

        return [control]


# EXPERIMENT SETUP
env = gym.make('Pendulum-v0')
env.reset()

# Give a set starting position
# This is done for reproducability
env = env.unwrapped
env.state = np.array([1.57, 0])

# Setup PID
pid = PID(target=0, Kp=0.6, Ki=0.2, Kd=0.7)
pid.setConstraints(2., -2.)

# initialise state and control
theta = 0.
controls = env.action_space.sample()
reward = 0.
error = 0.

# Start simulation loop
for _ in range(1000):
    print("Theta: {:.2f}  Error: {:.2f}  Control: {:.2f}".format(theta, error, controls[0]))
    env.render()
    state, reward, done, info = env.step(controls)
    theta = stateToAngle(state[0], state[1])

    # inverse the problem such that we're trying to
    # stabalise the pendulum to point downwards
    if theta > 0:
        theta = math.pi - theta
    else:
        theta = -math.pi - theta

    error = pid.calculateError(theta)
    controls = pid.control()

    # This is added so that it's easier to monitor things
    # time.sleep(0.1)

# stop environment
env.close()
