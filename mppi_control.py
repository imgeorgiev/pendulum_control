#!/usr/bin/env python3

# Simple inverted pendulum control using 
# Model Predictive Path Integral Control as inspired by
# the Information Theoretic MPC paper
# https://homes.cs.washington.edu/~bboots/files/Informationn_timestepsheoreticMPC.pdf

import gym
import numpy as np
import time
import math
from utils import stateToAngle


class MPPI():
    """ MPPI Algorithm"""

    def __init__(self, env, n_samples, n_timesteps, control_seq, lambda_=1.0, control_noise=1., u_init=0.):
        self.n_samples = n_samples
        self.n_timesteps = n_timesteps
        self.lambda_ = lambda_
        self.noise_sigma = control_noise
        self.control_seq = control_seq
        self.u_init = u_init  # initial control
        self.cost_total = np.zeros(shape=(self.n_samples))

        self.env = env
        self.x_init = self.env.env.state

        # Generate noise for control sequence
        self.noise = np.random.normal(0.0, scale=self.noise_sigma, size=(self.n_samples, self.n_timesteps))

    def compute_total_cost(self, sample_idx):
        """Cost-wise averaging of sampled trajectories"""
        self.env.env.state = self.x_init
        for t in range(self.n_timesteps):
            perturbed_action_t = self.control_seq[t] + self.noise[sample_idx, t]
            _, reward, _, _ = self.env.step([perturbed_action_t])
            self.cost_total[sample_idx] += -reward

    def ensure_non_zero(self, cost, beta, factor):
        """Ensure that costs never go to 0"""
        return np.exp(-factor * (cost - beta))

    def control(self):
        """Computer control sequence
        Returns the optimal control sequence"""
        for k in range(self.n_samples):
            self.compute_total_cost(k)

        beta = np.min(self.cost_total)  # minimum cost of all trajectories
        cost_total_non_zero = self.ensure_non_zero(cost=self.cost_total, beta=beta, factor=1/self.lambda_)

        eta = np.sum(cost_total_non_zero)
        omega = 1/eta * cost_total_non_zero

        self.control_seq += [np.sum(omega * self.noise[:, t]) for t in range(self.n_timesteps)]

        self.env.env.state = self.x_init
        s, r, _, _ = self.env.step([self.control_seq[0]])

        self.control_seq = np.roll(self.control_seq, -1)  # shift all elements to the left
        self.control_seq[-1] = self.u_init
        self.cost_total[:] = 0
        self.x_init = self.env.env.state
        return self.control_seq, s


# EXPERIMENT SETUP
env = gym.make('Pendulum-v0')
env.reset()

# Give a set starting position
# n_timestepshis is done for reproducability
env.env.state = np.array([np.pi, 1.])

# Setup MPPI
timesteps = 20
samples = 1000
control_sequence = np.random.uniform(low=-2., high=2., size=timesteps)  # pendulum joint effort in (-2, +2)
mppi = MPPI(env, samples, timesteps, control_sequence, 1., control_noise=5.)

for _ in range(1000):
    env.render()
    control_seq, state = mppi.control()
    theta = stateToAngle(state[0], state[1])
    print("Theta: {:.2f}  Control: {:.2f}".format(theta, control_seq[0]))
    env.render()

env.close()
