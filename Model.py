#!/usr/bin/env python3

# Model for the OpenAI Gym Pendulum
# Documentation: https://github.com/openai/gym/wiki/Pendulum-v0
# Source code: https://github.com/openai/gym/blob/master/gym/envs/classic_control/pendulum.py

import numpy as np


class Model():
    def __init__(self, dt=0.05, g=10, l=1, m=1):
        self.dt = dt
        self.g = g
        self.l = l
        self.m = m

    def step(self, state, control):
        theta = state[0]
        theta_dot = state[1]

        new_theta_dot = theta + (-3*self.g/(2*self.l) * np.sin(self.th + np.pi) + 3./(self.m*self.l**2)*control) * self.dt
        new_theta = theta + new_theta_dot*self.dt
        new_state = np.array([new_theta, new_theta_dot])
        return new_state

    def cost_fn(self, x):
        """ Cost function for the inverted pendulum
        x is numpy array of length 3
        x[0] - theta
        x[1] - theta dot
        x[2] - control velocity
        """
        return x[0]^2 + 0.1*x[1]^2 + 0.001*x^2

    def cost_fn_der(self, x):
        """ Derivative of the cost function wrt controls
        x is numpy array of length 1
        x[0] - control velocity
        """
        return 0.002*x[0]
