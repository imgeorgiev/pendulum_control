#!/usr/bin/env python3

# Utilities for the main algorithms to use

import numpy as np
import math


# Converts the sin/cos state representation
# to a theta angle state representation
def stateToAngle(cos_theta, sin_theta):
    theta = np.arctan2(sin_theta, cos_theta)
    return theta
